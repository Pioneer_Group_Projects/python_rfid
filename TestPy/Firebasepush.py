from firebase.firebase import FirebaseApplication, FirebaseAuthentication

SECRET = 'jOxJe49kT5VIKue4F4ULUheehHu2Oh9RVHfDefNv'
DSN = 'https://trackyt-01.firebaseio.com/'
EMAIL = 'samuelvinay.kumar@gmail.com'
authentication = FirebaseAuthentication(SECRET, EMAIL, True, True)
firebase = FirebaseApplication(DSN, authentication)


if __name__ == '__main__':
    import sys
    import datetime
    import serial
    import time
    import binascii
    import json
    from serial import SerialException
    reload(sys)
sys.setdefaultencoding('utf8')
try:
    serial = serial.Serial('COM5', baudrate=57600)
    # serial = serial.Serial('/dev/ttyUSB0', baudrate=57600)  # RPI3 Config
except SerialException:
    print ('port not connected to device or already connected')
while True:
    if serial.inWaiting() > 0:
        read_result = serial.read(16)
        hex_bytes = binascii.hexlify(read_result)
        # print("Read card {0}" . format(read_result.decode(encoding='ISO-8859-1')))
        # print(hex_bytes.encode('utf16'))
        hex_str = hex_bytes.decode('ascii')
        firstpart, secondpart = hex_str[:len(hex_str) / 2], hex_str[len(hex_str) / 2:]
        # print(firstpart)
        # print(secondpart)
        # data = {'RFID': hex_str[len(hex_str) / 2:], 'Created_time': datetime.datetime.now()}
        URL = '/RFIDInfo/' + hex_str[len(hex_str) / 2:]
        result = firebase.get(URL, None)
        # print result
        if not result:
            check = json.loads(json.dumps(result))
            # print check['RFID']
            # if check['RFID'] != hex_str[len(hex_str) / 2:]:
            snapshot = firebase.put(URL, 'RFID_Tag', hex_str[len(hex_str) / 2:])
            snapshot = firebase.put(URL, 'Created_time', datetime.datetime.now())
            # print ("Successfully Done")
            # print("Sleeping 1 second")
            time.sleep(1)
            serial.flushInput()  # ignore errors, no data

        else:
            # print("Data already exists in Firebase")

            URL = '/RFIDInfo/' + hex_str[len(hex_str) / 2:]
            datafetch = firebase.get(URL, None)
            # print datafetch
            firebase.put(URL, 'Updated_time', datetime.datetime.now())
            serial.flushInput()  # ignore errors, no data

            Tet1 = '/RFIDInfo/' + hex_str[len(hex_str) / 2:] + '/Created_time'
            Tet2 = '/RFIDInfo/' + hex_str[len(hex_str) / 2:] + '/Updated_time'
            CreatedTime = firebase.get(Tet1, None)
            UpdatedTime = firebase.get(Tet2, None)
            # print CreatedTime
            # print UpdatedTime

            # set the date and time format
            date_format = "%Y-%m-%dT%H:%M:%S.%f"

            # convert string to actual date and time
            time1 = datetime.datetime.strptime(CreatedTime, date_format)
            time2 = datetime.datetime.strptime(UpdatedTime, date_format)
            # print time1
            # print time2

            # find the difference between two dates
            diff = time2 - time1

            ''' days and overall hours between two dates '''
            # print ('Days & Overall hours from the above two dates')
            # print days
            days = diff.days
            # print (str(days) + ' day(s)')

            # print overall hours
            days_to_hours = days * 24
            diff_btw_two_times = (diff.seconds) / 3600
            overall_hours = days_to_hours + diff_btw_two_times
            # print (str(overall_hours) + ' hours');

            ''' now print only the time difference '''
            ''' between two times (date is ignored) '''

            # print ('\nTime difference between two times (date is not considered)')

            # like days there is no hours in python
            # but it has seconds, finding hours from seconds is easy
            # just divide it by 3600

            hours = (diff.seconds) / 3600
            # print (str(hours) + ' Hours')

            # same for minutes just divide the seconds by 60

            minutes = (diff.seconds) / 60
            # print (str(minutes) + ' Minutes')

            # to print seconds, you know already ;)

            # print (str(diff.seconds) + ' secs')

            URL = '/RFIDInfo/' + hex_str[len(hex_str) / 2:]
            num = 4
            if hours > num:
                firebase.put(URL, 'Flag', 'Yes')
            else:
                firebase.put(URL, 'Flag', 'No')

            URL = '/RFIDInfo/' + hex_str[len(hex_str) / 2:]
            # print datafetch
            firebase.put(URL, 'Diff_in_time', str(hours) + ' Hours')

            URL2 = '/RFIDInfo/' + hex_str[len(hex_str) / 2:] + '/Diff_in_time'
            Timediff = firebase.get(URL2, None)
            # print (Timediff)

            serial.flushInput()  # ignore errors, no data





